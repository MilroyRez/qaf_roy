package QAF_Package;
import java.io.File;
import org.junit.*;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.util.*;

public class Mixed_Scenarios  {
	private WebDriver driver;
	//String winHandleBefore = driver.getWindowHandle();

	@Before
	public void setUp() throws Exception {

		ChromeOptions co = new ChromeOptions();
		co.addArguments("--start-maximized");
		System.setProperty("webdriver.chrome.driver","C:/E25M Workspace/External JAR files/Chrome/chromedriver.exe");
		driver = new ChromeDriver(co);
	}
//
	@Test
	public void testSearch() throws Exception {

///==================================================================================================================////
				//  Single Product & user creation
///==================================================================================================================////
		driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
		driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
		Thread.sleep(2000);	       	 
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("10x10x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("1");
		driver.findElement(By.name("continue")).click();		                           
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[1]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[2]/form/div/button")).click();
		Thread.sleep(5000); 
		
		driver.findElement(By.id("billing:firstname")).sendKeys("AutoQAF");
		driver.findElement(By.id("billing:lastname")).sendKeys("Perera");
		driver.findElement(By.id("billing:street1")).sendKeys("1250 Capital of TX Hwy South, Bldg. 1 Ste 400");
		driver.findElement(By.id("billing:apt_ste")).sendKeys("7876");
		driver.findElement(By.id("billing:city")).sendKeys("Austin");
		new Select(driver.findElement(By.id("billing:region_id"))).selectByVisibleText("Texas");
		driver.findElement(By.id("billing:postcode")).sendKeys("78746");
		driver.findElement(By.id("billing:telephone")).sendKeys("0112458968");
		driver.findElement(By.id("billing:email")).sendKeys("autoqafuser@gmail.com");
		driver.findElement(By.id("billing:customer_password")).sendKeys("123456");
		driver.findElement(By.id("billing:confirm_password")).sendKeys("123456");

		driver.findElement(By.id("cryozonic_stripe_cc_owner")).sendKeys("Milroy Perera");
		driver.findElement(By.id("cryozonic_stripe_cc_number")).sendKeys("4242424242424242");
		driver.findElement(By.id("cryozonic_stripe_cc_cid")).sendKeys("123");
		new Select(driver.findElement(By.id("cryozonic_stripe_expiration"))).selectByVisibleText("01 - January");
		new Select(driver.findElement(By.id("cryozonic_stripe_expiration_yr"))).selectByVisibleText("2020");

		Thread.sleep(8000);  
		driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
		Thread.sleep(8000);


///==================================================================================================================////
		//  Multiple Products & fill CC details
///==================================================================================================================////

		driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
		driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
		Thread.sleep(2000);
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("10x20x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("1");
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[4]/div/div/div[1]/div[2]/div[1]/form/button")).click();		
		new Select(driver.findElement(By.id("filterSizeHide_2"))).selectByVisibleText("12x24x4");
		new Select(driver.findElement(By.id("qty2"))).selectByVisibleText("2");
		driver.findElement(By.name("continue")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	    driver.findElement(By.id("cryozonic_stripe_cc_owner")).sendKeys("Milroy Perera");
		driver.findElement(By.id("cryozonic_stripe_cc_number")).sendKeys("4242424242424242");
		driver.findElement(By.id("cryozonic_stripe_cc_cid")).sendKeys("123");
		new Select(driver.findElement(By.id("cryozonic_stripe_expiration"))).selectByVisibleText("01 - January");
		new Select(driver.findElement(By.id("cryozonic_stripe_expiration_yr"))).selectByVisibleText("2020");
		Thread.sleep(8000);  
		driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
		Thread.sleep(8000); 
		
		
///==================================================================================================================////
		//  Multiple Products // By using saved Credit card details
///==================================================================================================================////

		driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
		driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
		Thread.sleep(2000);
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("10x20x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("1");
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[4]/div/div/div[1]/div[2]/div[1]/form/button")).click();		
		new Select(driver.findElement(By.id("filterSizeHide_2"))).selectByVisibleText("16x25x5");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("1");
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[4]/div/div/div[1]/div[2]/div[1]/form/button")).click();		
		new Select(driver.findElement(By.id("filterSizeHide_3"))).selectByVisibleText("12x24x4");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("1");
		driver.findElement(By.name("continue")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(5000);
		driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[1]/div[3]/ol[2]/div/li/div[3]/dl/dd/ul/li[1]/ul/li[1]/input")).click(); 
		Thread.sleep(8000);  
		driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
		Thread.sleep(8000); 
		
///==================================================================================================================////
		//  Custom Products
///==================================================================================================================////    	

		driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div/div[1]/div/div/div[1]/ul[1]/li[4]/a")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div/div[1]/div/div[2]/ul/li[10]/a")).click();
		Thread.sleep(2000);
		driver.get("http://qaf.825magento.com/index.php/referafriend/index/broadcast/?refd=WVZGUHBPZldxbG1VT0RONytUMXBmUDRYcUMzWmxmVFNWNnYxYnRQRkRwMD0=");
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div/div[5]/div[4]/div/div/div[1]/div[2]/div[1]/form/a")).click();
		new Select(driver.findElement(By.xpath("/html/body/div[3]/div/div[5]/div[4]/div/div/div[1]/div[2]/div[1]/form/div[2]/div/div/div[1]/div/div/div[1]/div[1]/select[1]"))).selectByVisibleText("1");
		new Select(driver.findElement(By.xpath("/html/body/div[3]/div/div[5]/div[4]/div/div/div[1]/div[2]/div[1]/form/div[2]/div/div/div[1]/div/div/div[1]/div[2]/select[1]"))).selectByVisibleText("16");
		new Select(driver.findElement(By.xpath("/html/body/div[3]/div/div[5]/div[4]/div/div/div[1]/div[2]/div[1]/form/div[2]/div/div/div[1]/div/div/div[1]/div[3]/select[1]"))).selectByVisibleText("1");
		driver.findElement(By.xpath("/html/body/div[3]/div/div[5]/div[4]/div/div/div[1]/div[2]/div[1]/form/div[2]/div/div/div[3]/div/button")).click();
		driver.findElement(By.name("continue")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[5]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[5]/section[2]/div/div/div/div[3]/form/div/button")).click();
		Thread.sleep(5000); 		
		driver.findElement(By.id("billing:firstname")).sendKeys("Autousertwo");
		driver.findElement(By.id("billing:lastname")).sendKeys("Perera");
		driver.findElement(By.id("billing:street1")).sendKeys("1250 Capital of TX Hwy South, Bldg. 1 Ste 400");
		driver.findElement(By.id("billing:apt_ste")).sendKeys("7876");
		driver.findElement(By.id("billing:city")).sendKeys("Austin");
		new Select(driver.findElement(By.id("billing:region_id"))).selectByVisibleText("Texas");
		driver.findElement(By.id("billing:postcode")).sendKeys("78746");
		driver.findElement(By.id("billing:telephone")).sendKeys("0112458968");
		driver.findElement(By.id("billing:email")).sendKeys("royautotwo@gmail.com");
		driver.findElement(By.id("billing:customer_password")).sendKeys("123456");
		driver.findElement(By.id("billing:confirm_password")).sendKeys("123456");

		driver.findElement(By.id("cryozonic_stripe_cc_owner")).sendKeys("Milroy Perera");
		driver.findElement(By.id("cryozonic_stripe_cc_number")).sendKeys("4242424242424242");
		driver.findElement(By.id("cryozonic_stripe_cc_cid")).sendKeys("123");
		new Select(driver.findElement(By.id("cryozonic_stripe_expiration"))).selectByVisibleText("01 - January");
		new Select(driver.findElement(By.id("cryozonic_stripe_expiration_yr"))).selectByVisibleText("2020");
		Thread.sleep(8000);  
		driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
		Thread.sleep(8000);

 

///==================================================================================================================////
		//  Refer a friend link through
///==================================================================================================================////

		driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
		driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[1]/ul[1]/li[4]/a")).click();
		driver.findElement(	By.xpath("/html/body/div[3]/div/div[4]/div/div[1]/div/div[2]/ul/li[10]/a")).click();
		Thread.sleep(2000);
		driver.get("http://qaf.825magento.com/index.php/referafriend/index/broadcast/?refd=WVZGUHBPZldxbG1VT0RONytUMXBmUDRYcUMzWmxmVFNWNnYxYnRQRkRwMD0=");
		Thread.sleep(2000);
		driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
		Thread.sleep(2000);
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("10x10x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("1");
		driver.findElement(By.name("continue")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[5]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[5]/section[2]/div/div/div/div[3]/form/div/button")).click();
		Thread.sleep(5000); 
		driver.findElement(By.id("billing:firstname")).sendKeys("AutoRF");
		driver.findElement(By.id("billing:lastname")).sendKeys("Perera");
		driver.findElement(By.id("billing:street1")).sendKeys("1250 Capital of TX Hwy South, Bldg. 1 Ste 400");
		driver.findElement(By.id("billing:apt_ste")).sendKeys("7876");
		driver.findElement(By.id("billing:city")).sendKeys("Austin");
		new Select(driver.findElement(By.id("billing:region_id"))).selectByVisibleText("Texas");
		driver.findElement(By.id("billing:postcode")).sendKeys("78746");
		driver.findElement(By.id("billing:telephone")).sendKeys("0112458968");
		driver.findElement(By.id("billing:email")).sendKeys("referafriendone@gmail.com");
		driver.findElement(By.id("billing:customer_password")).sendKeys("123456");
		driver.findElement(By.id("billing:confirm_password")).sendKeys("123456");

		driver.findElement(By.id("cryozonic_stripe_cc_owner")).sendKeys("Milroy Perera");
		driver.findElement(By.id("cryozonic_stripe_cc_number")).sendKeys("4242424242424242");
		driver.findElement(By.id("cryozonic_stripe_cc_cid")).sendKeys("123");
		new Select(driver.findElement(By.id("cryozonic_stripe_expiration"))).selectByVisibleText("01 - January");
		new Select(driver.findElement(By.id("cryozonic_stripe_expiration_yr"))).selectByVisibleText("2020");
		Thread.sleep(8000);  
		driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
		Thread.sleep(8000);

	}

}