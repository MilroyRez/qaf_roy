package QAF_Package;
import java.io.File;

import org.junit.*;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.*;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.remote.Augmenter;


public class Signin_at_Checkout_Page  {
	private WebDriver driver;
	//String winHandleBefore = driver.getWindowHandle();
	
	@Before
	public void setUp() throws Exception {

		ChromeOptions co = new ChromeOptions();
		co.addArguments("--start-maximized");
		System.setProperty("webdriver.chrome.driver","C:/E25M Workspace/External JAR files/Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		}
			        
	@Test
	public void testSearch() throws Exception {
		
		
		driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);

	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	       	 
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("10x10x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("1");
		
		driver.findElement(By.name("continue")).click();
		                           
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[1]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[2]/form/div/button")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[1]/div[1]/div/div/a")).click();
		driver.findElement(By.id("id_onestepcheckout_username")).sendKeys("h.milroyperera@gmail.com");
		driver.findElement(By.id("id_onestepcheckout_password")).sendKeys("123456");
		driver.findElement(By.id("onestepcheckout-login-button")).click();
	//	driver.switchTo().window(winHandleBefore);
		 Thread.sleep(5000); 
		 
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	    driver.findElement(By.id("cryozonic_stripe_cc_owner")).sendKeys("Milroy Perera");
		driver.findElement(By.id("cryozonic_stripe_cc_number")).sendKeys("4242424242424242");
		driver.findElement(By.id("cryozonic_stripe_cc_cid")).sendKeys("123");
		new Select(driver.findElement(By.id("cryozonic_stripe_expiration"))).selectByVisibleText("01 - January");
		new Select(driver.findElement(By.id("cryozonic_stripe_expiration_yr"))).selectByVisibleText("2020");
		
	   // driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(8000);
	     
 
	  
	    ///==================================================================================================================////
 	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("10x20x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("1");
		
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[4]/div/div/div[1]/div[2]/div[1]/form/button")).click();		
		new Select(driver.findElement(By.id("filterSizeHide_2"))).selectByVisibleText("16x25x5");
		new Select(driver.findElement(By.id("qty2"))).selectByVisibleText("1");
		
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[4]/div/div/div[1]/div[2]/div[1]/form/button")).click();		
	    new Select(driver.findElement(By.id("filterSizeHide_3"))).selectByVisibleText("12x24x4");
		new Select(driver.findElement(By.id("qty3"))).selectByVisibleText("2");

		
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		/*	driver.findElement(By.id("cryozonic_stripe_cc_owner")).sendKeys("Milroy Perera");
		driver.findElement(By.id("cryozonic_stripe_cc_number")).sendKeys("4242424242424242");
		driver.findElement(By.id("cryozonic_stripe_cc_cid")).sendKeys("123");
		new Select(driver.findElement(By.id("cryozonic_stripe_expiration"))).selectByVisibleText("01 - January");
		new Select(driver.findElement(By.id("cryozonic_stripe_expiration_yr"))).selectByVisibleText("2020");*/
		
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(8000);  
	    
	    
	  ///==================================================================================================================////    
	    
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("10x20x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(8000);  
	 
 ///==================================================================================================================////

	     driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("10x24x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("4");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[3]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[4]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(4000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("10x30x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("4");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[4]/form/button")).click(); // 6month
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("12x12x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("5");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("12x16x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("12x18x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("12x20x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("12x24x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("12x30x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("12x36x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("14x14x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("14x18x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("14x20x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("14x24x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("14x25x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("14x30x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("15x20x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("15x30x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("16x16x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("16x20x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("16x24x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("16x25x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("16x30x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("18x18x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("18x20x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("18x22x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("18x24x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("18x25x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("18x30x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("20x20x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("20x21x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("20x22x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("20x24x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("20x25x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("20x30x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("20x36x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("22x22x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("24x24x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("24x30x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("25x25x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
	    
	    ///==================================================================================================================////
	     
	    driver.get("http://qaf.825magento.com/");
		Thread.sleep(2000);
	    driver.findElement(	By.xpath("/html/body/div[3]/div/div[1]/div/div/div[2]/a")).click();
	    Thread.sleep(2000);
	 				
		new Select(driver.findElement(By.id("filterSizeHide_1"))).selectByVisibleText("25x32x1");
		new Select(driver.findElement(By.id("qty1"))).selectByVisibleText("3");
		driver.findElement(By.name("continue")).click();

		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.id("card_1A3sPm4V4WPDM080mPGyLXLk")).click(); 
	    Thread.sleep(8000);  
	    driver.findElement(By.id("onestepcheckout-button-place-order")).click(); 
	    Thread.sleep(5000); 
	    
//=============================================================================================================================================//	    
	                                                             // 2inch Thick //
	    ///==================================================================================================================////
	}

}

